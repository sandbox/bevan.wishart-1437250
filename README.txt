-- SUMMARY --
Provides a hook_output_alter(&$page) hook.

$page contains the fully rendered output of the page. make  any alterations you like.

